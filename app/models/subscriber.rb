class Subscriber < ActiveRecord::Base
  has_and_belongs_to_many :cameras
  has_secure_password

  before_create :create_token

  def update_token
    self.update_attribute :token, generate_token
  end

  private
  def create_token
    self.token ||= generate_token
  end

  def generate_token
    SecureRandom.uuid.tr('-', '')
  end
end
