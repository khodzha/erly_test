class Camera < ActiveRecord::Base
  belongs_to :client
  has_and_belongs_to_many :subscribers

  validates :access, inclusion: { in: %w(private public authorized) }
  validates :streamer, presence: true

  def rtsp_url
    @rtsp_url ||= "rtsp://#{streamer}/#{name}?"
  end

  def hls_url
    @hls_url ||= "hls://#{streamer}/#{name}/index-m3u8?"
  end
end
