json.array! @cameras do |camera|
  json.(camera, :id, :name, :dvr_depth, :has_thumbnails, :is_persistent)
  json.(camera, :access, :comment, :title, :streamer, :postal_address)

  json.location [camera.lat, camera.long]
  json.auth_token SecureRandom.hex(8)

  json.status ({alive: false, thumbnail: false})

  json.urls ({rtsp: camera.rtsp_url, hls: camera.hls_url})
end
