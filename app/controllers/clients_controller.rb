class ClientsController < BaseController
  before_action :find_client, only: [:edit, :update, :destroy]
  def index
    @clients = Client.all
  end

  def show
    @client = Client.includes({cameras: :subscribers}).find(params[:id])
  end

  def new
    @client = Client.new
  end

  def create
    @client = Client.new client_params
    if @client.save
      redirect_to action: :show
    else
      render 'new'
    end
  end

  def edit
  end

  def update
    if @client.update_attributes client_params
      redirect_to action: :show
    else
      render 'edit'
    end
  end

  def destroy
    @client.destroy
    redirect_to action: :index
  end

  private
  def client_params
    params.require(:client).permit(:name)
  end

  def find_client
    @client = Client.find params[:id]
  end
end
