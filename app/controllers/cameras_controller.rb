class CamerasController < BaseController
  def show
    @camera = Camera.includes(:subscribers).find(params[:id])
  end
end
