class SessionsController < ApplicationController
  def new
  end

  def create
    admin = Admin.find_by!(email: params[:email])
    if admin.authenticate(params[:password])
      session[:admin_id] = admin.id
      redirect_to root_url
    else
      @error = 'Authentication failed'
      render :new
    end
  end

  def destroy
    session[:admin_id] = nil
    redirect_to root_url
  end
end
