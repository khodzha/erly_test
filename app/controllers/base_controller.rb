class BaseController < ApplicationController
  before_action :authenticate_admin

  private
  def authenticate_admin
    redirect_to new_session_url unless current_admin
  end
end
