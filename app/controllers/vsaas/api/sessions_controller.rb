class Vsaas::Api::SessionsController < Vsaas::Api::BaseController
  skip_before_action :authenticate_subscriber, only: :create

  def create
    subscriber = Subscriber.find_by!(email: params[:email])
    if subscriber.authenticate(params[:password])
      subscriber.update_token
      render json: { success: true, session: subscriber.token }
    else
      render json: { success: false }, status: 422
    end
  end
end
