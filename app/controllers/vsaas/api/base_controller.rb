class Vsaas::Api::BaseController < ::ApplicationController
  before_action :authenticate_subscriber
  skip_before_action :verify_authenticity_token

  private
  def authenticate_subscriber
    render json: { error: 'Unathorized' }, status: 401 unless current_subscriber
  end

  def current_subscriber
    token = request.headers["x-vsaas-session"]
    @current_subscriber ||= Subscriber.find_by(token: token) if token.present?
  end
end
