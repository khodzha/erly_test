class Vsaas::Api::CamerasController < Vsaas::Api::BaseController
  def index
    @cameras = current_subscriber.cameras
  end
end
