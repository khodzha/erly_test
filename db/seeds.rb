Admin.create! email: 'admin@example.com', password: '123123'

%w(peter john matthew).each do |name|
  client = Client.create! name: name
  client.cameras = 2.times.map{ Camera.create name: SecureRandom.hex(3), streamer: %w(vice vox ya tj).sample }
end

%w(washington clinton bush roosevelt lincoln jefferson carter reagan).each do |name|
  subs = Subscriber.create name: name, email: "#{name}@example.com", password: '123123'
  subs.cameras = Camera.all.shuffle.take(rand(7))
end
