# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150730140125) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "admins", force: :cascade do |t|
    t.string   "email",           null: false
    t.string   "password_digest", null: false
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "admins", ["email"], name: "index_admins_on_email", unique: true, using: :btree

  create_table "cameras", force: :cascade do |t|
    t.integer  "client_id"
    t.string   "name"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.integer  "dvr_depth"
    t.boolean  "has_thumbnails", default: false,     null: false
    t.boolean  "is_persistent",  default: false,     null: false
    t.string   "access",         default: "private", null: false
    t.text     "comment"
    t.string   "title"
    t.string   "streamer"
    t.decimal  "lat"
    t.decimal  "long"
    t.string   "postal_address"
  end

  add_index "cameras", ["client_id"], name: "index_cameras_on_client_id", using: :btree

  create_table "cameras_subscribers", id: false, force: :cascade do |t|
    t.integer "camera_id",     null: false
    t.integer "subscriber_id", null: false
  end

  create_table "clients", force: :cascade do |t|
    t.string   "name",       null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "subscribers", force: :cascade do |t|
    t.string   "name",            null: false
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.string   "email"
    t.string   "password_digest"
    t.string   "token"
  end

  add_index "subscribers", ["email"], name: "index_subscribers_on_email", unique: true, using: :btree

  add_foreign_key "cameras", "clients"
end
