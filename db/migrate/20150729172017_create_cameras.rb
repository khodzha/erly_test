class CreateCameras < ActiveRecord::Migration
  def change
    create_table :cameras do |t|
      t.references :client, index: true, foreign_key: true
      t.string :name

      t.timestamps null: false
    end
  end
end
