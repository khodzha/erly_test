class CreateJoinTableCameraSubscriber < ActiveRecord::Migration
  def change
    create_join_table :cameras, :subscribers do |t|
      # t.index [:camera_id, :subscriber_id]
      # t.index [:subscriber_id, :camera_id]
    end
  end
end
