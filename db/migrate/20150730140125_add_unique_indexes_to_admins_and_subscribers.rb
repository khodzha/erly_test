class AddUniqueIndexesToAdminsAndSubscribers < ActiveRecord::Migration
  def change
    add_index :admins, :email, unique: true
    add_index :subscribers, :email, unique: true
  end
end
