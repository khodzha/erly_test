class AddExtraFieldsToCameras < ActiveRecord::Migration
  def change
    add_column :cameras, :dvr_depth, :integer

    add_column :cameras, :has_thumbnails, :boolean, null: false, default: false
    add_column :cameras, :is_persistent, :boolean, null: false, default: false

    add_column :cameras, :access, :string, null: false, default: 'private'
    add_column :cameras, :comment, :text
    add_column :cameras, :title, :string
    add_column :cameras, :streamer, :string, null: false

    add_column :cameras, :lat, :decimal
    add_column :cameras, :long, :decimal

    add_column :cameras, :postal_address, :string
  end
end
