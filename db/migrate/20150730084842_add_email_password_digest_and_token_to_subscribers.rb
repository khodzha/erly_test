class AddEmailPasswordDigestAndTokenToSubscribers < ActiveRecord::Migration
  def change
    add_column :subscribers, :email, :string
    add_column :subscribers, :password_digest, :string
    add_column :subscribers, :token, :string
  end
end
