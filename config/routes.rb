Rails.application.routes.draw do
  resources :clients
  resources :cameras, only: [:show]

  resources :sessions, only: [:new, :create] do
    delete :destroy, on: :collection
  end

  namespace :vsaas do
    namespace :api, constraints: { format: :json } do
      post 'login' => 'sessions#create'
      get 'my/cameras' => 'cameras#index'
    end
  end

  root to: 'clients#index'
end
